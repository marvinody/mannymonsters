exports = {};

const status = require('./Status')
const {readFileSync, statSync, readdirSync} = require('fs');
const fight = exports.fight = function(monsters, rng, starting_idx = 0 ){
  //determines who starts attacking first
  let idx = starting_idx;
  //just niceties, prev prevents negative indices also
  const next_idx = () => (opts.idx+1)%monsters.length
  const prev_idx = () => ((opts.idx-1)%monsters.length+monsters.length)%monsters.length

  const opts = {
    turn_cnt: 0,
    rng,
    starting_idx,
    idx,
  }

  {
    //block scope cause this don't need to be public
    //init the monsters to load any hooks
    const m1 = monsters[opts.idx];
    const m2 = monsters[next_idx()];
    m1.init(m2, opts);
    m2.init(m1, opts);
  }

  while(monsters[opts.idx].canFight()){
    let attacking = monsters[opts.idx];
    let def_idx = next_idx();
    let defending = monsters[def_idx];

    move_status = attacking.doMove(defending, opts);

    switch(move_status){
      case status.TURN_DONE:
      //do nothing cause we don't need to
      break;
      case status.TURN_EXTRA:
      //roll back index by one
      //console.log(`EXTRA TURN FOR ${attacking.colorName()}`)
      opts.idx = prev_idx();
      break;


    }
    opts.idx = next_idx();
    opts.turn_cnt += 1;
  }

  let loser = monsters[opts.idx],
  winner = monsters[prev_idx()];
  return {winner, loser, opts};
}, RNG = exports.RNG = class RNG{
  constructor(seed = 1){
    this.seed = seed
  }
  int(l=0,h=10000){
    return Math.floor(
      (h-l)*this.rand() + l
    );
  }
  //range of [0,1)
  rand(){//https://stackoverflow.com/questions/521295
    let x = Math.sin(this.seed++) * 10000;
    return x - Math.floor(x);
  }
  consecHeads(max=10){
    let idx = 0;
    //while heads and we haven't capped
    while (this.int(0,2) > 0 && idx < max){
      idx += 1;
    }

    return idx;
  }


}, rotate = exports.rotate = (arr) => arr.push(arr.shift()),
average = exports.average = (arr) => arr.reduce((acc,e)=>acc+e,0)/arr.length,
EmitterProxy = exports.EmitterProxy = class EmitterProxy{
  //https://stackoverflow.com/questions/43944274
  constructor(emitter) {
    // add method that has access to emitter
    this.on = function(event, fn) {
      let self = this;
      return emitter.on(event, function() {
        // this makes sure that the value of this is the EmitterProxy
        // instance and not the emitter instance
        fn.apply(self, arguments);
      });
    }

    // define any other methods you want to proxy
  }
}, color_obj = exports.color_obj = {
  GREY: "\x1b[30m",
  RED: "\x1b[31m",
  GREEN: "\x1b[32m",
  YELLOW: "\x1b[33m",
  BLUE: "\x1b[34m",
  PURPLE: "\x1b[35m",
  CYAN: "\x1b[36m",
  WHITE: "\x1b[37m",
}, colors = exports.colors = Object.values(color_obj),
reset_color = exports.reset_color = "\x1b[0m",
getAllMonsters = exports.getAllMonsters = () => {
  let obj = {};
  const path = require("path").join(__dirname, "Monsters");
  readdirSync(path)
  .forEach(file => {
    let fullPath = require("path").join(path, file)
    const stats = statSync(fullPath);
    if(stats.isFile() && file.substr(-3) === '.js'){
      //if it's a js file, then just return it cause good enough
      obj[file.slice(0,-3)] = require("./Monsters/" + file);
    } else if(stats.isDirectory()){
      //otherwise, let's dig in a bit more
      //gonna make some assumptions like that folder will have 2 files in root
      try {
        const js_path = require("path").join(__dirname, "Monsters",file,`${file}.js`);

        const stats = statSync(js_path);
        if(stats.isFile() && js_path.substr(-3) === '.js'){
          let module_path = `./Monsters/${file}/${file}.js`;
          obj[file] = require(module_path);
        }
      } catch (err) {
        console.log(err);
        return false;
      }
    }

  })
  return obj;
}, fillKeysWithZero = exports.fillKeysWithZero = function() {
  obj = {};
  Object.values(arguments).forEach(arg => obj[arg] = 0)
  return obj;
}, NotNumberException = exports.NotNumberException = function(message){
  this.message = message;
  Error.captureStackTrace(this, NotNumberException);
}, NotValidCounterTypeException = exports.NotValidCounterTypeException = function(message){
  this.message = message;
  Error.captureStackTrace(this, NotValidCounterTypeException);
}, isNumeric = exports.isNumeric = n => !isNaN(n)
, cachedMMC = {}
, loadMMCIfExists = exports.loadMMCIfExists = f => {
  try {
    if(cachedMMC[f]){
      return cachedMMC[f];
    }
    const path = require("path").join('Monsters/',f,`/${f}.mmc`)
    const content = readFileSync(path, 'utf8');
    let lines = content.split(/[\r\n]+/);
    let obj = {}, cur_obj = obj;
    const regex = /(\w+):(.*)/;
    lines.forEach(line => {
      let trimmed = line.trim();
      //comment
      if(trimmed[0] == ';') {return;}
      if(trimmed[0] == '[') {
        let header = trimmed.slice(1).trim();
        obj[header] = {};
        cur_obj = obj[header];
        return;
      }
      if ((m = regex.exec(trimmed)) !== null) {
        if(m.length !== 3) {return;}
        let key = m[1].trim();
        let value = m[2].trim();
        if(isNumeric(value)){
          value = parseInt(value,10);
        }
        cur_obj[key] = value;
      }

    });
    cachedMMC[f] = obj;
    return obj;

  } catch (err){
    if(err.code === 'ENOENT'){
      cachedMMC[f] = {};
      return {};
    } else {
      throw err;
    }
  }

}

NotNumberException.prototype = Object.create(Error.prototype);
NotNumberException.prototype.name = "NotNumberException";
NotNumberException.prototype.constructor = NotNumberException;

NotValidCounterTypeException.prototype = Object.create(Error.prototype);
NotValidCounterTypeException.prototype.name = "NotValidCounterTypeException";
NotValidCounterTypeException.prototype.constructor = NotValidCounterTypeException;

module.exports = exports;
