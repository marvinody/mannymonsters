const Monster = require('../../Monster')
module.exports = class Loli extends Monster {
  constructor(){
    super();
    this.old_hp = this.hp;

    //this.moves.push(this.fail);
    this.moves.push(this.wink);
    this.moves.push(this.placeholder);
    this.moves.push(this.hug);
    this.moves.push(this.kissu);
  }
  init(enemy, opts){
    this.addFlag('invuln');
    enemy.addCounter('refreshing', 'charm');
    enemy.addFlag('charmed');

    /*Deals with the invuln part for loli*/
    enemy.addEffect('preAttack',() => {
      this.old_hp = this.hp;
    });
    enemy.addEffect('postAttack',() => {
      if(this.flags.invuln.state){

        this.flags.invuln.set(false);
        this.hp = this.old_hp;
      }
    });
    /*Deals with the charm part and damage redirection*/
    enemy.addEffect('preAttack', (loli, dmg, opts) => {
      if(enemy.flags.charmed.state){
        loli.dealDamage(enemy, dmg, opts);
        return {dmg:0, priority: 1}
      }
    })
    enemy.addEffect('postAttack', (loli, dmg, opts) => {
      if(enemy.flags.charmed.state){
        //if we get needed amt, uncharm
        const heads_needed = this.mmc.charm.heads_needed;
        const state = !(opts.rng.consecHeads(heads_needed) == heads_needed);
        enemy.flags.charmed.state = state;
      }
    })
  }
  attempt_charm(e){
    if(e.counters.charm.cnt >= this.mmc.charm.cnt_needed){
      e.flags.charmed.state = true;
    }
  }
  fail(){}
  wink(e, opts){
    e.counters.charm.inc();
  }
  placeholder(e, opts){
    this.dealDamage(e, this.mmc.dmg.placeholder, opts);
    e.counters.charm.inc();
    this.attempt_charm(e);
  }
  hug(e, opts){
    e.counters.charm.inc();
    this.attempt_charm(e);
    this.dealDamage(e, this.mmc.dmg.hug, opts);
    this.flags.invuln.set(true);
  }
  kissu(e, opts){
    this.dealDamage(e, this.mmc.dmg.kissu, opts);
    e.flags.charmed.set(true);
  }

}
