const Monster = require('../../Monster'),
Status = require('../../Status');
module.exports = class PlaceHold extends Monster {
  constructor(){
    super();

    this.addCounter('stacking', 'extra_turn');

    this.moves.push(this.fail);
    this.moves.push(this.dank);
    this.moves.push(this.dank);
    this.moves.push(this.meme1);
    this.moves.push(this.meme2);
  }
  fail(){}
  dank(e, opts){
    this.dealDamage(e, this.mmc.dmg.dank, opts);
    if(this.counters.extra_turn.cnt > 0){
      this.counters.extra_turn.dec();
      return Status.TURN_EXTRA;
    }
  }
  meme1(e, opts){
    this.counters.extra_turn.refresh(this.mmc.extra_turn.meme1);
    return Status.TURN_EXTRA;
  }
  meme2(e, opts){
    this.counters.extra_turn.refresh(this.mmc.extra_turn.meme2);
    return Status.TURN_EXTRA;
  }

}
