const Monster = require('../../Monster'),
Status = require('../../Status');
module.exports = class Ratking extends Monster {

  constructor(){
    super();

    this.addCounter('stacking', 'rats');

    this.moves.push(this.recruit);
    this.moves.push(this.ratbash);
    this.moves.push(this.ratGoons);
    this.moves.push(this.mutatedRatGoons);
    this.moves.push(this.ratRevolution);
  }
  recruit(e, opts){
    this.counters.rats.refresh(this.mmc.rats.recruit);
  }
  ratbash(e, opts){
    let dmg = this.mmc.dmg.ratbash;
    this.dealDamage(e, dmg, opts);
    this.counters.rats.refresh(this.mmc.rats.ratbash);
  }
  ratGoons(e, opts){
    let dmg = 0;
    let ratChange = 0;
    switch(this.counters.rats.cnt){
      case 0:
        dmg = this.mmc.dmg.ratGoons_0;
        ratChange = this.mmc.rats.ratGoons_0;
        break;
      case 1:
        dmg = this.mmc.dmg.ratGoons_1;
        ratChange = this.mmc.rats.ratGoons_1;
        break;
      default://not 0 or 1, so 2 or greater
        dmg = this.mmc.dmg.ratGoons_2p;
        ratChange = this.mmc.rats.ratGoons_2p;
    }
    this.dealDamage(e, dmg, opts);
    this.counters.rats.refresh(ratChange);
  }
  mutatedRatGoons(e, opts){
    let dmg = 0,
    ratChange = 0
    status = Status.TURN_DONE;
    switch(this.counters.rats.cnt){
      case 0 :
        dmg = this.mmc.dmg.mRG_0;
        ratChange = this.mmc.rats.mRG_0;
        status = Status.TURN_EXTRA;
        break;
      case 1:
        dmg = this.mmc.dmg.mRG_1;
        ratChange = this.mmc.rats.mRG_1;
        break;
      default:
        let rats = this.counters.rats.cnt;
        dmg = this.mmc.dmg.mRG_2p_base + (this.mmc.dmg.mRG_2p_multi * rats);
        this.counters.rats.reset();
    }
    this.dealDamage(e, dmg, opts);
    this.counters.rats.refresh(ratChange);
    return status;
  }
  ratRevolution(e, opts){
    const rats = this.counters.rats.cnt;
    if(rats == 0){
      this.heal(e, this.mmc.heal.ratRev_0, opts);
    } else if (rats == 1){
      this.dealDamage(e, this.mmc.dmg.ratRev_1, opts);
    } else if (rats >= 2 && rats <= 5){
      this.dealDamage(e, e.hp, opts);
    } else {
      this.heal(e,this.mmc.heal.retRev_6p,opts);
    }
  }

}
