const Monster = require('../Monster')
module.exports = class TriHard extends Monster {
  constructor(){
    super();
    this.hp = 10;
    this.sneak = 0;
    //this.moves.push(this.fail);
    this.moves.push(this.Jordans);
    this.moves.push(this.SerratedDagger);
    this.moves.push(this.EmergencyWatermelon);
    this.moves.push(this.MonkeyNoises);
    this.moves.push(this.MineNow);
  }
  init(enemy, _){
    enemy.addEffect('preAttack',(_,dmg,opts)=>{
      if(this.sneak > 0){
        if(opts.rng.consecHeads(1)==0){
          return {dmg:this.round(dmg/2),priority:1}
        }
      }
    })
  }
  fail(){}
  Jordans(e, opts){
    if(this.sneak>0){
      this.sneak += 3;
    } else {
      this.sneak += 4;
      this.heal(e, -1, opts);
    }

  }
  SerratedDagger(e, opts){
    let dmg = 1;
    if(this.sneak > 0){
      dmg = 2;
      this.sneak -= 1;
    }
    this.dealDamage(e, dmg, opts);
    this.heal(e, dmg, opts);
  }
  EmergencyWatermelon(e, opts){
    if(this.sneak > 0){
      this.heal(e, 3, opts);
      this.sneak -= 1;
    } else {
      this.heal(e, 2, opts);
    }
  }
  MonkeyNoises(e, opts){
    if(this.sneak > 0){
      let dmg = 2 * this.sneak + 2;
      this.dealDamage(e, dmg, opts);
      this.sneak = 0;
    }
  }
  MineNow(e, opts){
    if(e.moves.length == 5){
      e.moves[4] = this.fail
    }
    this.sneak +=5;
  }

}
