const Monster = require('../Monster')
module.exports = class Emo extends Monster {
  constructor(){
    super();
    this.hp = 1;

    this.moves.push(this.lose);
    this.moves.push(this.win);
  }
  lose(e, opts){
    this.hp = 0;
  }
  win(e, opts){
    e.hp = 0;
  }

}
