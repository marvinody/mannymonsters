const Monster = require('../../Monster')
module.exports = class Muscles extends Monster {
  constructor(){
    super();

    this.moves.push(this.add_one);
    this.moves.push(this.add_two);
    this.moves.push(this.POGGERS);
  }
  init(){
    this.addCounter('stacking', 'extra_dmg');
  }
  add_one(e){
    this.counters.extra_dmg.refresh(this.mmc.extra_dmg.add_one_bonus);
  }
  add_two(e){
    this.counters.extra_dmg.refresh(this.mmc.extra_dmg.add_two_bonus);
  }
  POGGERS(e, opts){
    const base_dmg = this.mmc.dmg.POGGERS_base;
    const total = base_dmg + this.mmc.extra_dmg.multiplier * this.counters.extra_dmg.cnt;
    this.dealDamage(e, total, opts);
    this.counters.extra_dmg.reset();
  }

}
