const Monster = require('../Monster')
module.exports = class GeoMon extends Monster {
  constructor(){
    super();
    this.hp = 10;

    this.moves.push(this.atk);
  }
  atk(e, opts){
    this.dealDamage(e, 2, opts);
  }

}
