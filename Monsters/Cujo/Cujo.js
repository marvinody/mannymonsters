const Monster = require('../../Monster')
module.exports = class Cujo extends Monster {
  constructor(){
    super();

    this.addCounter(this.mmc.dmg_rdc.type, 'dmg_rdc');

    this.moves.push(this.howl);
    this.moves.push(this.bite);
    this.moves.push(this.bite);
    this.moves.push(this.devour);
    this.moves.push(this.bloodrush);

  }
  init(enemy, opts){
    this.addEffect('preTurn',() => {
      this.get.dmg_rdc.dec();
    });

    enemy.addEffect('preAttack', (_, dmg, opts)=>{
      if(this.get.dmg_rdc.cnt > 0){
          return {
            dmg:dmg-this.mmc.dmg_rdc.amt,
            priority:1
          };
      }
    })
  }
  howl(e){
    this.get.dmg_rdc.inc();
  }
  bite(e,opts){
    let dmg = this.mmc.dmg.bite;
    this.dealDamage(e, dmg, opts);
  }
  devour(e, opts){
    let dmg = this.mmc.dmg.devour;
    let heal = this.mmc.heal.devour;
    this.dealDamage(e, dmg, opts);
    this.heal(e, heal, opts);
  }
  bloodrush(e, opts){
    let dmg = this.mmc.dmg.bloodrush_high;
    if (this.hp <= this.mmc.bldrush_cutoff.low_hp){
      dmg = this.mmc.dmg.bloodrush_low;
    } else if (this.hp <= this.mmc.bldrush_cutoff.mid_hp){
      dmg = this.mmc.dmg.bloodrush_mid;
    }
    this.dealDamage(e, dmg, opts);
  }

}
