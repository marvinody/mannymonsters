const Monster = require('../../Monster')
module.exports = class Cerberos extends Monster {
  constructor(){
    super();

    this.moves.push(this.fail);
    this.moves.push(this.bite);
    this.moves.push(this.bite);
    this.moves.push(this.claw);
    this.moves.push(this.swallow);
  }
  fail(e, opts){
    return;
  }
  bite(e, opts){
    let dmg = this.mmc.dmg.bite
    this.dealDamage(e,dmg,opts);
  }
  claw(e, opts){
    let dmg = this.mmc.dmg.claw;
    this.dealDamage(e,dmg,opts);
  }
  swallow(e, opts){
    let dmg = e.hp;
    this.dealDamage(e,dmg,opts);
  }

}
