const Monster = require('../../Monster')
module.exports = class ZenTurtle extends Monster {
  constructor(){
    super();

    this.addCounter(this.mmc.invuln.type, 'invuln');

    this.moves.push(this.rest);
    this.moves.push(this.headbutt);
    this.moves.push(this.shell_spin);
    this.moves.push(this.cowabunga);
  }
  init(e, opts){
    this.addEffect('preTurn',function(){
        this.get.invuln.dec();
    })
    e.addEffect('postAttack',() => {
      if(this.hp <= 0 && this.get.invuln.cnt > 0){
        this.hp = 1;
      }
    });
  }
  fail(){}
  rest(e, opts){
    if(this.get.invuln.cnt == 0){
      this.heal(e, this.mmc.heal.rest, opts);
    }
  }
  headbutt(e, opts){
    this.dealDamage(e, this.mmc.dmg.headbutt, opts);
    this.heal(e, this.mmc.heal.headbutt, opts);
  }
  shell_spin(e, opts){
    this.dealDamage(e, this.mmc.dmg.shell_spin, opts);
    this.heal(e, this.mmc.heal.shell_spin, opts);
  }
  cowabunga(e, opts){
    this.get.invuln.refresh(this.mmc.invuln.refresh);
  }


}
