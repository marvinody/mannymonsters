const Monster = require('../../Monster'),
status = require('../../Status')
module.exports = class Devilbat extends Monster {
  constructor(){
    super();

    this.moves.push(this.wingChop);
    this.moves.push(this.fire);
    this.moves.push(this.hypnoticEyeBeam);
    this.moves.push(this.bloodsucking);
  }
  wingChop(e,opts){
    this.dealDamage(e,this.mmc.dmg.wingChop,opts);
  }
  fire(e,opts){
    this.dealDamage(e,this.mmc.dmg.fire,opts);
  }
  hypnoticEyeBeam(e,opts){
    this.dealDamage(e,this.mmc.dmg.hypnoticEyeBeam,opts);
    return status.TURN_EXTRA;
  }
  bloodsucking(e,opts){
    this.dealDamage(e,this.mmc.dmg.bloodsucking,opts);
    this.heal(e,this.mmc.heal.bloodsucking,opts);
  }

}
