const status = require('./Status');
const utils = require('./Utils');
//https://stackoverflow.com/questions/9781218
const reset_color = "\x1b[0m";

module.exports = class Monster{
  constructor(){
    this.moves = [];
    this._hp = 0;
    //map of stack objects and flag objects, added with a method below only
    this._counters = {};
    this._flags = {};
    this.effects = {
      preTurn:[],
      preAttack:[],
      postAttack:[],
      postTurn:[],
    }
    this.mmc = utils.loadMMCIfExists(this.name);
    if(this.mmc.hp){
      this._hp = this.mmc.hp;
    }
  }
  /*Getter and setter for hp because we really don't want negative hp*/
  get hp(){
    return this._hp;
  }
  set hp(newHp){
    this._hp = Math.max(0, newHp);
  }
  get get(){
    return this._counters;
  }
  get counters(){
    return this._counters;
  }
  get flags(){
    return this._flags;
  }
  init(e, opts){
    //this is where you would initialize any hooks you need later
  }
  round(n){
    //incase wanna change how rounding works later
    return Math.ceil(n)
  }
  get name(){
    return this.constructor.name;
  }

  numMoves(){
    return this.moves.length;
  }

  heal(e, orig_heal, opts){
    //no hooks yet but may change at a later point
    if(!utils.isNumeric(orig_heal)){
      throw new utils.NotNumberException(`Heal must be a number, not ${orig_heal}`);
    }
    let cur_heal = orig_heal, cur_priority = 0;
    //need to allow damage mod incase we need it
    //also need to allow multiple effects and higher priority takes efffect
    this.effects.preAttack.forEach(hdlr => {
      //both of these might be undefined, or only 1 might be
      const {heal, priority} = hdlr.call(this, e, orig_heal, opts) || {}
      if(priority >= cur_priority){//undef >= any int is always false
        cur_heal = heal || cur_heal; // undef || any int is always any int
      }
    });
    
    this.hp += cur_heal;

    //dont need to do that here cause no point modifying anything after
    //only stat tracking or something like recoil
    this.effects.postAttack.forEach(hdlr => hdlr.call(this, e, cur_heal, opts));
  }
  takeDamage(dmg){
    this.hp = this.round(this.hp - dmg);
  }
  dealPercentDamage(){
    throw ReferenceError('Function not defined');
  }
  dealDamage(e, orig_dmg, opts){
    if(!utils.isNumeric(orig_dmg)){
      throw new utils.NotNumberException(`Damage must be a number, not ${orig_dmg}`);
    }
    let cur_dmg = orig_dmg, cur_priority = 0;
    //need to allow damage mod incase we need it
    //also need to allow multiple effects and higher priority takes efffect
    this.effects.preAttack.forEach(hdlr => {
      //both of these might be undefined, or only 1 might be
      const {dmg, priority} = hdlr.call(this, e, orig_dmg, opts) || {}
      if(priority >= cur_priority){//undef >= any int is always false
        cur_dmg = dmg || cur_dmg; // undef || any int is always any int
      }
    });
    if (cur_dmg < 0 ){
      cur_dmg = 0; //to prevent healing enemy
    }

    e.takeDamage(cur_dmg);
    //dont need to do that here cause no point modifying anything after
    //only stat tracking or something like recoil
    this.effects.postAttack.forEach(hdlr => hdlr.call(this, e, cur_dmg, opts));
  }

  addFlag(name, state = false){
    if(this.flags[name]){
      throw new Error(`Counter '${name}' already exists`);
    }
    this.flags[name] = new Flag(state);
    return this.flags[name];

  }

  addCounter(type, name){
    if(this.counters[name]){
      throw new Error(`Counter '${name}' already exists`);
    }
    if(type === 'stacking'){
      this.counters[name] = new StackingCounter(name);
    } else if (type === 'refreshing'){
      this.counters[name] = new RefreshingCounter(name);
    } else {
      throw new utils.NotValidCounterTypeException(`Type must be 'stacking' or 'refreshing', not ${type}`);
    }
  }

  canFight(){
    return this.hp > 0;
  }

  addEffect(hook, hdlr){
    if(!this.effects[hook] || typeof hdlr != 'function'){
      return;
    }
    this.effects[hook].push(hdlr);
  }

  doMove(enemy, opts){
    const orig_idx = opts.rng.consecHeads(this.moves.length-1);
    //console.log(`${this.colorName()} Attacks ${enemy.colorName()} with ${this.moves[idx].name}`)
    //same idea here as the damage ones, only with idx
    let cur_idx = orig_idx, cur_priority = 0;

    this.effects.preTurn.forEach(hdlr => {
      const {idx, priority} = hdlr.call(this, enemy, cur_idx, opts)  || {}
      if(priority >= cur_priority){//undef >= any int is always false
        cur_idx = idx || cur_idx; // undef || any int is always any int
      }
    });
    const move = this.moves[cur_idx];
    opts.move_idx = cur_idx;
    //move might not return status so turn done if nothing
    const orig_move_status = move.call(this, enemy, opts) || status.TURN_DONE;
    let cur_move_status = orig_move_status

    this.effects.postTurn.forEach(hdlr => {
      const {move_status, priority} = hdlr.call(this, enemy, orig_move_status, opts)  || {}
      if(priority >= cur_priority){//undef >= any int is always false
        cur_move_status = move_status || cur_move_status; // undef || any int is always any int
      }
    });
    return cur_move_status
  }
}

class Flag {
  constructor(state = false){
    this._state = state;
  }
  get state(){
    return this._state;
  }
  set(newState){
    this.state = newState;
    return this;
  }
  set state(newState){
    if(newState === true || newState === false){
      this._state = newState;
    } else {
      throw new Error(`Invalid state supplied: ${newState} to Flag`);
    }
  }

}

class BaseCounter {
  constructor(name){
    this.name = 0;
    this.count = 0;
  }
  get cnt(){
    return this.count;
  }
  dec(){
    if(this.count > 0){
      this.count -= 1;
    }
    return this;
  }
  inc(){
    this.count += 1;
    return this;
  }
  reset(){
    this.count = 0;
  }
}
class StackingCounter extends BaseCounter {
  constructor(name){
    super(name);
  }
  refresh(n){
    if(!utils.isNumeric(n)){
      throw new utils.NotNumberException(`Refresh amt must be a number, not ${n}`);
    }
    this.count += n;
    return this;
  }
}
class RefreshingCounter extends BaseCounter {
  constructor(name){
    super(name);
  }
  refresh(n){
    if(!utils.isNumeric(n)){
      throw new utils.NotNumberException(`Refresh amt must be a number, not ${n}`);
    }
    this.count = n;
    return this;
  }
}
