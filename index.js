//Code from Manny
status = require('./Status')
utils = require('./Utils')

const num_fights = +process.argv[2] || 100000,
results = {},
all_monsters  = utils.getAllMonsters();

if(process.argv.length == 3){
  if(all_monsters[process.argv[2]]){
    results[process.argv[2]] = utils.fillKeysWithZero.apply(null, Object.keys(all_monsters))
  }
} else if(process.argv.length == 4){
  if(utils.isNumeric(process.argv[2])){
    results[process.argv[3]] = utils.fillKeysWithZero.apply(null, Object.keys(all_monsters))
  } else {
    results[process.argv[2]] = utils.fillKeysWithZero.apply(null, Object.keys(all_monsters))
    results[process.argv[3]] = utils.fillKeysWithZero.apply(null, Object.keys(all_monsters))
  }
} else if (process.argv.length == 5){
  let m1_str = process.argv[3],
  m2_str = process.argv[4]
  results[m1_str] = utils.fillKeysWithZero(m1_str, m2_str);
  results[m2_str] = utils.fillKeysWithZero(m1_str, m2_str);
}

//if we haven't touched results yet
if(Object.keys(results).length === 0){
  Object.keys(all_monsters).forEach((c,ind,arr) => {
    results[c] = utils.fillKeysWithZero.apply(null, arr)
    /*Object.keys(all_monsters).forEach(c_2 => {
      results[c][c_2] = 0;
    })*/
  })
}





starting_seed = Math.random()*100000|0;
starting_seed = 66235


Object.keys(results).forEach((monster_1, idx_1)=>{
  Object.keys(results[monster_1]).forEach((monster_2, idx_2) => {
    //if we're doing all the battles, we can skip half cause obvious
    //but if we're doing limited match, can't skip unless square
    if(Object.keys(results).length === Object.keys(all_monsters).length && idx_1 >= idx_2){
      return;
    }
    if(monster_1 === monster_2){
      return;
    }
    let starting_idx = 0;
    const monsterConstructors = [monster_1, monster_2];
    for(let j=0;j<2;j++){
      const rng = new utils.RNG(starting_seed);
      for(let i=0;i<num_fights/2;i++){
        //console.log(`--------FIGHT--------`);
        //const monsters = monsterConstructors.map(m => new all_monsters[m]());
        const monsters = [new all_monsters[monsterConstructors[0]](), new all_monsters[monsterConstructors[1]]()]
        const {winner, loser, opts} = utils.fight(monsters, rng, starting_idx);
        if(results[winner.name]){
          results[winner.name][loser.name] += 1;
        }

      }
      starting_idx = 1
    }
  })
})

process.stdout.write('Table\n');

const colors = utils.colors,
color_obj = utils.color_obj,
reset_color = utils.reset_color;
//create headline
//headline dependent on 'losers'. grab first toplevel and check all losers
{
  let first_mon = Object.keys(results)[0]
  process.stdout.write('\t  ');
  Object.keys(results[first_mon]).forEach((monster, idx)=>{
    mon = monster.substring(0,8).padStart(8);
    process.stdout.write(`${colors[idx%colors.length]}${mon}${reset_color}|`)
  })
  process.stdout.write('\n');
}


//create rows
process.stdout.write('Winners\n')
Object.keys(results).forEach((monster, idx)=>{
  let mon = monster.substring(0,9).padStart(9),
  mon_color = colors[idx%colors.length];
  process.stdout.write(`${mon_color}${mon}${reset_color}|`)
  Object.keys(results[monster]).forEach((loser, idx)=>{
    let wins = results[monster][loser],
    score_color = color_obj.RED;
    if(wins > num_fights / 2 ){
      score_color = color_obj.GREEN;
    } else if (wins === 0 ){
      score_color = mon_color;
    }
    process.stdout.write(`${score_color}${wins.toString().padStart(8)}${reset_color}|`)
  })
  process.stdout.write('\n');
})
